import { LAPTOP_API_URL, ROOT_URL } from "./utils.js"

// DOM ELEMENTS:
const btnGetLoan = document.getElementById('btn-get-loan')
const btnTransferBank = document.getElementById('btn-transfer-bank')
const btnWork = document.getElementById('btn-work')
const btnRepayLoan = document.getElementById('btn-repay-loan')
const btnBuy = document.getElementById("btn-buy-now")
const bankBalanceDisplay = document.getElementById('bank-balance')
const outstandingLoanDisplay = document.getElementById('outstanding-loan')
const salaryAmountDisplay = document.getElementById('salary')
const loanContainer = document.getElementById('loan-container')
const laptopSelect = document.getElementById("laptop-select")
const laptopImage = document.getElementById("laptop-image")
const laptopName = document.getElementById("laptop-name")
const laptopDescription = document.getElementById("laptop-description")
const laptopPrice = document.getElementById("laptop-price")
const featureList = document.getElementById("feature-list")
const features = document.getElementById("features")

// VARIABLES:
let bankBalance = 1000 // Initial bank balance
let outstandingLoan = 0 // Initial outstanding loan amount
let hasLoan = false // Flag to track if a loan has been taken
let salaryAmount = 0 // Initialize variable salary amount

// EVENT LISTENERS:
btnGetLoan.addEventListener('click', getLoan)
btnTransferBank.addEventListener('click', transferToBank)
btnWork.addEventListener('click', work)
btnRepayLoan.addEventListener('click', repayLoan)
btnBuy.addEventListener("click", () => buyNow())
laptopSelect.addEventListener("change", () => {
    updateBtnBuyVisibility()
    updateLaptopInfo()
})

// EVENT HANDLERS:
// Function to handle the "Get a loan" button click
function getLoan() {
    // You may not have two loans at once
    if (hasLoan) {
        alert('You already have an outstanding loan. Please repay the previous loan before taking a new one.')
        return
    }

    // User clicked Cancel
    let loanAmount = prompt('Enter the loan amount:')
    if (loanAmount === null) {
        return
    }

    // Checks if enter information is valid positive number
    const parsedLoanAmount = parseFloat(loanAmount)
    if (isNaN(parsedLoanAmount) || parsedLoanAmount <= 0) {
        alert('Invalid loan amount. Please enter a positive number.')
        return
    }

    // You cannot get a loan more than double of your bank balance
    if (parsedLoanAmount > bankBalance * 2) {
        alert('Loan amount exceeds the limit. You cannot get a loan more than double your bank balance.')
        return
    }

    bankBalance += parsedLoanAmount
    outstandingLoan = parsedLoanAmount
    hasLoan = true
    updateBankBalance()
    updateOutstandingLoan()

    // Show the loan container
    loanContainer.style.display = 'flex'

    // Update the "Repay Loan" button visibility
    updateRepayButtonVisibility()
}

// Function to handle the "Bank" button click
function transferToBank() {
    // Check if there is any salary to transfer
    if (salaryAmount === 0) {
        alert("You don't have any salary to transfer.")
        return
    }

    if (outstandingLoan > 0) {
        // Deduct 10% of salary amount for loan repayment (if applicable)
        const deductedAmount = Math.floor(salaryAmount * 0.1)
        outstandingLoan -= deductedAmount
        bankBalance += (salaryAmount - deductedAmount)

        // If loan amount becomes negative, add the absolute value to bank balance
        if (outstandingLoan < 0) {
            bankBalance += Math.abs(outstandingLoan)
            outstandingLoan = 0
            hasLoan = false
            loanContainer.style.display = 'none'
            updateRepayButtonVisibility()
        }
    } else {
        bankBalance += salaryAmount
    }

    // Reset salary amount, and update displays
    salaryAmount = 0
    updateBankBalance()
    updateSalaryAmount()
    updateOutstandingLoan()
}

// Function to handle the "Work" button click
function work() {
    // Increase salary amount by 100 and update display
    salaryAmount += 100
    updateSalaryAmount()
}

// Function to handle the "Repay loan" button click
function repayLoan() {
    if (salaryAmount <= 0) {
        alert("You don't have any salary to repay loan!")
        return
    }
    const confirmRepay = confirm("Are you sure you want to use your salary to repay the loan?")
    if (!confirmRepay) {
        return
    }

    // Use salary amount to repay the loan
    if (salaryAmount >= outstandingLoan) {
        salaryAmount -= outstandingLoan
        outstandingLoan = 0
        hasLoan = false
        loanContainer.style.display = 'none'
        updateRepayButtonVisibility()
    } else {
        outstandingLoan -= salaryAmount
        salaryAmount = 0
    }
    updateOutstandingLoan()
    updateSalaryAmount()
}

// Function to handle the "Buy Now" button click
const buyNow = () => {
    const selectedLaptopId = laptopSelect.value
    if (selectedLaptopId === "") {
        return // No laptop selected, do nothing
    }

    const LAPTOP_API_URL = `https://hickory-quilled-actress.glitch.me/computers/${selectedLaptopId}`
    fetch(LAPTOP_API_URL)
        .then(response => {
            if (!response.ok) {
                throw new Error("Error fetching laptop info!")
            }
            return response.json()
        })
        .then(laptop => {
            const laptopPrice = laptop.price
            if (bankBalance >= laptopPrice) {
                // Checks if yoy afford to buy the laptop
                bankBalance -= laptopPrice
                updateBankBalance()
                alert(`Congratulations! You are now the owner of a new laptop!`)
            } else {
                alert("Sorry, you cannot afford this laptop.")
            }
        })
        .catch(error => {
            console.error(error)
        })
}

// Function to update the visibility of the "Buy Now" button
const updateBtnBuyVisibility = () => {
    const selectedLaptopId = laptopSelect.value
    if (selectedLaptopId === "") {
        btnBuy.style.display = "none" // Hide the button if no laptop is selected
    } else {
        btnBuy.style.display = "block" // Show the button if a laptop is selected
    }
}

// Function to update the laptop info based on the selected laptop
const updateLaptopInfo = async () => {
    const selectedLaptopId = laptopSelect.value
    const LAPTOP_API_URL = `${ROOT_URL}/computers/${selectedLaptopId}`

    try {
        // Fetch laptop info based on the selected laptop
        const response = await fetch(LAPTOP_API_URL)
        if (!response.ok) {
            throw new Error("Error fetching laptop info!")
        }
        const laptop = await response.json()
        const imageUrl = `${ROOT_URL}/${laptop.image}`

        laptopImage.style.visibility = "visible"
        laptopImage.src = imageUrl
        laptopName.textContent = laptop.title
        laptopDescription.textContent = laptop.description
        laptopPrice.textContent = `${laptop.price} NOK`
        laptopPrice.style.fontWeight = "bold"
        laptopPrice.style.textAlign = "center"
        laptopPrice.style.fontSize = "22px"

        features.style.visibility = "visible"
        features.style.fontWeight = "bold"
        features.style.fontSize = "18px"
        features.style.marginBottom = "0px"

        featureList.innerHTML = ""
        featureList.style.padding = "0"
        featureList.style.marginTop = "0"

        // Create list items for each laptop spec and add them to the feature list
        laptop.specs.forEach(spec => {
            const listItem = document.createElement("li")
            listItem.textContent = spec
            featureList.appendChild(listItem)
            listItem.style.listStyleType = "none" // Removes dots

        })
    } catch (error) {
        console.error(error)
    }
}

// FUNCTIONS:
// Function to update the bank balance display
function updateBankBalance() {
    bankBalanceDisplay.textContent = bankBalance + " Kr."
    bankBalanceDisplay.style.textAlign = "right"
}

// Function to update the outstanding loan display
function updateOutstandingLoan() {
    outstandingLoanDisplay.textContent = outstandingLoan + " Kr."
}

// Function to update the salary amount display
function updateSalaryAmount() {
    salaryAmountDisplay.textContent = salaryAmount + " Kr."
}

// Function to update the "Repay Loan" button visibility
function updateRepayButtonVisibility() {
    if (hasLoan) {
        btnRepayLoan.style.display = 'inline-block'
    } else {
        btnRepayLoan.style.display = 'none'
    }
}

// Function to populate the laptop selection dropdown
const populateLaptops = async () => {
    try {
        // Fetch laptop data
        const response = await fetch(LAPTOP_API_URL)
        if (!response.ok) {
            throw new Error("Error fetching laptops!")
        }
        const data = await response.json()

        // Get the laptop select element from the DOM
        laptopSelect.innerHTML = `<option disabled selected>Select a laptop</option>`
        laptopSelect.style.fontSize = "20px"

        // Create option elements and add them to the laptop select element
        data.forEach(laptop => {
            const option = document.createElement("option")
            option.value = laptop.id
            option.text = laptop.title
            laptopSelect.appendChild(option)
        });
    } catch (error) {
        console.error(error)
    }
}

updateBankBalance()
updateSalaryAmount()
populateLaptops() // Populate laptop selection dropdown on page load
